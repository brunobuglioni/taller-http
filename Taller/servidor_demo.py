import socket
import json

print("Servidor HTTP")

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind(('127.0.0.1', 9100))
s.listen(1)

while True:
    client_connection, client_address = s.accept()
    request = client_connection.recv(1024)
    request = request.split('\r\n')
    method = request[0].split(' ')
    path = method[1]
    protocol = method[2]
    method = method[0]
    diccionario = {"path": path, "protocol": protocol, "method": method}
    cabezeras = {}
    
    for cabezera in request[1:]:
        try:
            cabezera = cabezera.split(': ')
            cabezeras[cabezera[0]] = cabezera[1]
        except IndexError:
            pass
    diccionario['headers'] = cabezeras
    try:
        file = open('documentRoot' + path)
        outputdata = file.read()
        file.close()
        client_connection.send("HTTP/1.1 200 OK\r\n")
        client_connection.send("X-RequestEcho: {0}\r\n"
                               .format(json.dumps(diccionario)))
        client_connection.send(outputdata)
        client_connection.close()
    except IOError:
        client_connection.send('HTTP/1.1 404 Not Found\r\n')
        client_connection.send("X-RequestEcho: {0}\r\n"
                               .format(json.dumps(diccionario)))
        client_connection.close()
